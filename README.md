
# Marine vs Zerg

You have to survive from zerg!

Player basically have a gun. Bullets are unlimited. 

If you kill enemies, you can acquire an item probabilistically.

There is a limit to the number of items, so use them strategically!

### ITEMS
    * Dynamite
        * Dynamite is explosive. Flame pits form on the explosion site and continuously damage the enemies.

    * Shark Bullet.
        * This bullet is very powerful so that it penetrates all enemies.

This game is divided into several rounds.

As the round moves on, new enemies emerge and the number increases.

### Enemies
    * Zergling
        * This is basic unit. Be careful, Weak when alone, Strong when together!

    * Mutallisk
        * Be careful to approach this! As you get closer, it shoots poisonous insect!

    * Poisonous insect
        * It's rolling around.

### How To Play the Game
    <Space> Go to the next round.
    <Arrow Key> Move.
    <Z> Shoot a bullet.
    <X> Use Dynamite.
    <C> Use Shark Bullet.



**** *The very basic structure of the code is based on HW4.*
