/*
	Project 1
	Name of Project: Marine vs Zerg
	Author: Sunghyun Yang
	Date:2020.05.29
*/
Player myPlayer;
Game game;
EnemyFactory f;
ArrayList<Bullet> bullets;
ArrayList<Enemy> enemies;
ArrayList<Item> items;

int width = 800;
int height = 800;

PImage bg;
PImage sb;
int bullet2;
int bullet3;
Rounds rounds;
int enemyNumber;
int score;
int highestScore;

//gameState = 0  -> waiting for the next round
//gameState = 1  -> Next round Called. Enemies creating before round starts
//gameState = 2  -> Round in progress. If it ends, gameState becomes 0
//gameState = 4  -> gameOver.
int gameState;

void setup()
{
  score = 0;
  gameState = 0;
  size(800,800);
  bg = loadImage("background.png");
  sb = loadImage("statusbar.png");
  bullets = new ArrayList<Bullet>(); 
  enemies = new ArrayList<Enemy>();
  items = new ArrayList<Item>();
  game = new Game();
  f = new EnemyFactory();
  myPlayer = new Player(300,300,80,80,3,200);
  bullet2 = 3;
  bullet3 = 3;
  rounds = new Rounds();
  rounds.draw();
  highestScore = 0;
}

void draw()
{
  background(bg);
  
  rounds.draw();
  //wait for the next round
  if(gameState == 0)
  {
    fill(255);
    textSize(35);
    game.hitTest();
    text("Press Space for the Round"+rounds.getRound(),100,100);
  }
  //enemies creating
  if(gameState == 1)
  {
    fill(255);
    textSize(35);
    text("Round "+rounds.getRound(),100,200);
  }
  //round starts
  if(gameState == 2)
  {
    game.updateAndDraw();
    game.hitTest();
    if(myPlayer.getHealth() <=0)
    {
      myPlayer.pHealth =0;
      gameState =4;
    }
  }
  //game ends
  if(gameState == 4)
  {
    fill(255);
    textSize(35);
    text("Game Over",100,100);
    text("Your Score : "+score,100,140);
    text("Press Space to Restart",100,180);
    if(highestScore < score) highestScore = score;
  }
  game.itemDraw();
  myPlayer.updatePositionAndDraw();
  image(sb,400,700,800,200);
  game.statusBar();
  
}

void keyPressed()
{
  //press space to start next round
  if(gameState == 0 )
  {
    if(key ==' ')
    gameState =1;
  }
  //press space to restart game
  if( gameState == 4)
  {
    if(key ==' ')
    {
      gameState =0;
      myPlayer.setHealth(200);
      score = 0;
    }
  }
  //press z,x,c to shoot bullet
  if(gameState == 2)
  {
    if(key == 'z')
    {
      game.shootBullet(1,myPlayer.getDirection(),myPlayer.getX(),myPlayer.getY(),10,25);
    }
    if(key == 'x')
    {
      if(bullet2>0)
      {
        game.shootBullet(2,myPlayer.getDirection(),myPlayer.getX(),myPlayer.getY(),20,50);
        bullet2 -=1;
      }
    }  
    if(key == 'c')
    {
      if(bullet3>0)
      {
        game.shootBullet(3,myPlayer.getDirection(),myPlayer.getX(),myPlayer.getY(),45,75);
        bullet3 -=1;
      }
    } 
  }
  if(key ==CODED)
  {
    if(keyCode == LEFT)
    {
      myPlayer.pLeft = true;
    }
    else if(keyCode == RIGHT)
    {
      myPlayer.pRight = true;
    }
    else if(keyCode == UP)
    {
      myPlayer.pUp = true;
    }
    else if(keyCode == DOWN)
    {
      myPlayer.pDown = true;
    }
  }
  
}

void keyReleased()
{
  if(key == CODED)
  {
    if(keyCode == LEFT)
    {
      myPlayer.pLeft = false;
    }
    if(keyCode == RIGHT)
    {
      myPlayer.pRight = false;
    }
    if(keyCode == UP)
    {
      myPlayer.pUp = false;
    }
    if(keyCode == DOWN)
    {
      myPlayer.pDown = false;
    }
  }
}


class Game 
{
  void statusBar()
  {
    fill(255);
    textSize(15);
    text("Health : "+myPlayer.getHealth(),200,700);
    text("Score : "+score,200,730);
    text("Highest Score : "+highestScore,200,760);
    fill(255);
    textSize(25);
    text(bullet2,690,770);
    text(bullet3,760,770);
  }
  //shoot a bullet
  void shootBullet(int bulletType,int direction, float x, float y,float w, float h)
  {
      if(bulletType == 1)
      bullets.add(new Bullet1(x,y,w,h,direction,10,50));
      if(bulletType == 2)
      bullets.add(new Dynamite(x,y,w,h,direction,10,100));
      if(bulletType == 3)
      bullets.add(new Shark(x,y,w,h,direction,7,20));
  }
  
  //update the bullets & enemie' statement and draw
  void updateAndDraw()
  {
    for(Bullet bullet : bullets) 
    {        
      //update the bullets' movement depends on Player's direction
      //in this case, I didn't use setter method because it decreases the frame rate.
      if(bullet.getVisible())
      {
        if(bullet.getDirection()==0)
        {
        bullet.y -= bullet.getSpeed();
        }      
        else if(bullet.getDirection()==1)
        {
        bullet.y -= bullet.getSpeed()*0.7;
        bullet.x += bullet.getSpeed()*0.7;
        }
        else if(bullet.getDirection()==2)
        {
        bullet.x += bullet.getSpeed();
        }
        else if(bullet.getDirection()==3)
        {
        bullet.y += bullet.getSpeed()*0.7;
        bullet.x += bullet.getSpeed()*0.7;
        }
        else if(bullet.getDirection()==4)
        {
        bullet.y += bullet.getSpeed();
        }
        else if(bullet.getDirection()==5)
        {
        bullet.y += bullet.getSpeed()*0.7;
        bullet.x -= bullet.getSpeed()*0.7;
        }
        else if(bullet.getDirection()==6)
        {
        bullet.x -= bullet.getSpeed();
        }
        else if(bullet.getDirection()==7)
        {
        bullet.y -= bullet.getSpeed()*0.7;
        bullet.x -= bullet.getSpeed()*0.7;
        }
        bullet.bSpeed -= bullet.getDamper();
        bullet.draw();
      }

      //BulletType : 2      Dynamite's Explosiion
      if(bullet.getVisible() && bullet.getSpeed() <0 && bullet.getType() == 2)
      {
        bullets.add(new Explosion(bullet.getX(),bullet.getY(),130,90,8,0,5));
        bullet.setVisible(false);
        return;
      }

      //out of map boundary
      if(bullet.getX()<0 ||bullet.getX()>width||bullet.getY()<0|| bullet.getY()>height)
      {
        bullet.setVisible(false);
      }
    }
    for(Enemy enemy : enemies) 
    {
      //EnemyType : 2        mutallisk shoot Bug when player approaches
      if(enemy.getType() ==2 && enemy.getVisible())
      {
        if(abs(enemy.getX()-myPlayer.getX())+abs(enemy.getY()-myPlayer.getY())<300)
        {
          enemies.add(new Bug(enemy.getX(),enemy.getY(),40,40,2,10,100));
          enemy.setType(4);
          enemyNumber +=1;
          return;
        }
      }
      
      //update the enemies' movement
      //Also in this case, I didn't use setter method because it decreases the frame rate.
      if(enemy.getVisible())
      {
        enemy.setDirection(enemy.getX()-myPlayer.getX(),enemy.getY()-myPlayer.getY());
        enemy.x -=enemy.getSpeed()*enemy.getDirection().x;
        enemy.y -=enemy.getSpeed()*enemy.getDirection().y;
        enemy.draw();
      }
    }
  }

  //items draw separately with bullets&enemies, because items remains on the floor until next round starts
  void itemDraw()
  {
    for(Item item : items)
    {
      if(item.getVisible())item.draw();
    }
  }

  //collision check among enemies, player, bullets, items
  void hitTest()
  {
    // hitTest with enemies <-> Player
    for (Enemy enemy : enemies)
    {
      if(enemy.getVisible())
      {
        if(enemy.getX()+enemy.getW()/2 > myPlayer.getX()-myPlayer.getW()/2+15 && enemy.getX()-enemy.getW()/2 < myPlayer.getX()+myPlayer.getW()/2-15)
        {
          if(enemy.getY()+enemy.getH()/2 > myPlayer.getY()-myPlayer.getH()/2+15 && enemy.getY()-enemy.getH()/2 < myPlayer.getY()+myPlayer.getH()/2-15)
          {
            myPlayer.pHealth -= enemy.getDamage();

            enemyNumber -=1;
            enemy.setVisible(false);
          }
        }
      }
    }

    // hitTest with enemies <-> bullets
    for(Bullet bullet : bullets)
    {
      if(bullet.getVisible())
      {
        for(Enemy enemy : enemies)
        {
          if(enemy.getVisible())
          {
            if(abs(enemy.getX()-bullet.getX())<enemy.getW()/2+bullet.getW()/2 && abs(enemy.getY()-bullet.getY())<enemy.getH()/2+bullet.getH()/2)
            {
              if(bullet.getType() == 1)
              {
                enemy.eHealth -= bullet.getDamage();
                bullet.setVisible(false);
              }
              if(bullet.getType() == 2)
              {
                enemy.eHealth -= bullet.getDamage();
                bullet.setVisible(false);
                bullets.add(new Explosion(bullet.getX(),bullet.getY(),130,90,8,0,5));
                return;
              }
              if(bullet.getType() == 3)
              {
                enemy.eHealth -= bullet.getDamage();
                bullet.count -= 1;
                if(bullet.count <=0) bullet.setVisible(false);
              }
              if(bullet.getType()==4)
              {
                enemy.eHealth -= bullet.getDamage();
              }
              
              //enemies died.
              if(enemy.getHealth()<=0)
              {
                int x;
                x = int(random(0,10));
                enemy.setVisible(false);enemyNumber -=1;
                score += 100;
                if(x ==1 )
                  items.add(new ItemDynamite(enemy.getX(),enemy.getY(),20,50));
                else if( x== 2)
                  items.add(new ItemShark(enemy.getX(),enemy.getY(),30,50));
              }
            }
          }
        }
      }
    }

    // hitTest with items <-> myPlayer
    for(Item item : items)
    {
      if(item.getVisible())
      {
        if(item.getX()+item.getW()/2 > myPlayer.getX()-myPlayer.getW()/2+15 && item.getX()-item.getW()/2 < myPlayer.getX()+myPlayer.getW()/2-15)
        {
          if(item.getY()+item.getH()/2 > myPlayer.getY()-myPlayer.getH()/2+15 && item.getY()-item.getH()/2 < myPlayer.getY()+myPlayer.getH()/2-15)
          {
            item.setVisible(false);
            if(item.getType() ==2) bullet2++;
            if(item.getType() ==3) bullet3++;

          }
        }
      }
    }
  }
}


class Rounds extends Thread
{
  int round = 1;
  Rounds(){start();}
  int getRound(){return round;}
  void draw(){
  }
  void run() {
    while(true)
    {
      //wait until round Starts
      while(gameState == 0)
      {
        try {
          Thread.sleep (100);
        } catch (Exception e) {}
      }

      //create enemies before round Starts
      if(gameState ==1)
      {
        bullets = new ArrayList<Bullet>(); 
        enemies = new ArrayList<Enemy>();
        items = new ArrayList<Item>();

        for(int i = 0; i<round; i++)
        {
          enemies.add(f.createZergling(0-150*i,int(random(0, 600))));
          enemies.add(f.createZergling(800+150*i,int(random(0,600))));
          if(i>1)
          {
            enemies.add(f.createZergling(int(random(0, 800)),0-150*(i-2)));
            enemies.add(f.createZergling(int(random(0, 800)),700+150*(i-2)));
          }
          if(i>3)
          {
            enemies.add(f.createMutallisk(0-150*(i-2),int(random(0, 600))));
            enemies.add(f.createMutallisk(800+150*(i-2),int(random(0, 600))));
          }
          if(i>5)
          {
            enemies.add(f.createMutallisk(int(random(0, 800)),0-150*(i-4)));
            enemies.add(f.createMutallisk(int(random(0, 800)),700+150*(i-4)));
          }
        }
        try {
          Thread.sleep (2000);
        } catch (Exception e) {}

        gameState = 2;
      }

      //wait until round Ends
      while(gameState == 2)
      {
        try {
          Thread.sleep (100);
        } catch (Exception e) {}
        if(enemyNumber <=0)
        {
          gameState =0; 

          round ++;
        }
      }
    }
  }
}
